const STS = require("ali-oss").STS;
const express = require("express");
const app = express();

const stsClient = new STS({
  // 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
  accessKeyId: 'LTAI5tKU5MjZWg4F1vFHoEYB',
  accessKeySecret: 'cCXzFkiYSIn925OSM3C8rUJHnb0ABz',
  // 填写Bucket名称。
  bucket: "chi-images",
});

async function getToken() {
  // 指定角色的ARN，格式为acs:ram::$accountID:role/$roleName。
  const STS_ROLE = "acs:ram::1430289004664265:role/ramosstest";
  const STSpolicy = {
    Statement: [
      {
        Action: ["oss:*"],
        Effect: "Allow",
        Resource: ["acs:oss:*:*:*"],
      },
    ],
    Version: "1",
  };
  const result = await stsClient.assumeRole(
    STS_ROLE,
    STSpolicy,
    3600 // STS过期时间，单位为秒。
  );
  const { credentials } = result;

  return credentials;
}

app.get("/getToken", async (req, res) => {
  // 获取STS。
  const credentials = await getToken();
  console.log(credentials.AccessKeyId);
  console.log(credentials.AccessKeySecret);
  console.log(credentials.SecurityToken);
  res.json(credentials);
});