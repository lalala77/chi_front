
const app = getApp()
const imageCdn = '../../images';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    selected:[],
    canteens: [],
    curNav: 1,
    curIndex: 0,
    dishes: [],
    count:0
  },

  //事件处理函数
  switchRightTab: function (e) {
    console.log(e)
    // 获取item项的id，和数组的下标值
    let id = e.target.dataset.id,
      index = parseInt(e.target.dataset.index);
    this.setData({
      curNav: id,
      curIndex: index
    })
    this.selectCanteen(id)
  },
  handleCheck(event) {
    const item = event.currentTarget.dataset.item;
    if(!this.selected){
      this.selected = []
    } 
    const index = this.selected.findIndex(selectedItem => selectedItem.id === item.id);
    if (index > -1) {
      this.selected.splice(index, 1);
    } else {
      this.selected.push(item);
    }

    console.log(this.selected)
    const count = this.selected.length
    this.setData({
      count:count
    })
  },

  // 选择食堂id，加载菜品信息
  selectCanteen(id) {
    var that = this
    wx.request({
      url: 'http://localhost:9998/dish/query',
      data: {
        "cid": id
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          dishes: res.data.result
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that = this
    wx.request({
      url: 'http://localhost:9998/canteen',
      method: 'GET',
      success: function (res) {
        that.setData({
          canteens: res.data.result
        })
      }
    })
    // 初始化加载第一食堂的菜品信息
    this.selectCanteen(1)
  
  },

  jumpToDecision(){
    wx.navigateTo({
      url: '/pages/decision/decision?params='+ JSON.stringify(this.selected),
    })
  }
});