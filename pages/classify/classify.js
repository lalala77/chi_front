// pages/classify/classify.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    stickyProps: {
      zIndex: 2,
    },
    page: 0,
    xianlinArr:[],
    gulouArr:[],
    dishes: [],
    display_dishes:[],
    canteenName:[],
    prices:[],
    cid:0,
    index1:[0,1,2,3,4,5,6],
    index2:[7,8,9,10],
    translateX: 0, // 位移x坐标 单位px
    translateY: 0, // 位移y坐标 单位px
    distance: 0, // 双指接触点距离
    scale: 1, // 缩放倍数
    rotate: 0, // 旋转角度
    oldRotate: 0, // 上一次旋转停止后的角度
    startMove: { // 起始位移距离
      x: 0,
      y: 0,
    },
    startTouches: [] // 起始点touch数组

  },


  touchMove(e) {
    const touches = e.touches
    const { pageX: onePageX, pageY: onePageY } = touches[0]
    const { scale, distance: oldDistance, startTouches } = this.data
    if (touches.length === 2 && startTouches.length === 2) {
      // 双指缩放
      const { pageX: twoPageX, pageY: twoPageY } = touches[1]
      // 求出当前双指距离
      const distance = Math.sqrt((twoPageX - onePageX) ** 2 + (twoPageY - onePageY) ** 2)
      this.data.distance = distance
      this.setData({
        scale: scale * (distance / (oldDistance || distance))
      })
    }
  },


   
  
    // 选择食堂id，加载菜品信息
    selectCanteen(id) {
      var that = this
      var save = this
      let prices = []
      wx.request({
        url: 'http://localhost:9998/dish/query',
        data: {
          "cid": id
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            dishes: res.data.result
          })

          that.data.dishes.forEach(element=>{
            prices.push(element.score.toFixed(1))
        })
        that.setData({
          prices: prices
        })

        var canteenName = []
        wx.request({
          url: 'http://localhost:9998/canteen',
          method: 'GET',
          success: function (res) {
            save.data.dishes.forEach(element => {
              canteenName.push(res.data.result[element.cid - 1].name)
            });
            save.setData({
              canteenName:canteenName
            })
            
          }
        })


        }
      })
    },


  onTabsChange(event) {
    console.log(event)
    this.setData({
      page:event.detail.value,
      cid:0
    })
    if(event.detail.value == 0){
      this.selectCanteen(this.data.xianlinArr[0].id)
    } else{
        this.selectCanteen(this.data.gulouArr[0].id)
    }
  },
  
  onTabsChange1(event) {
    this.setData({
      cid:event.detail.value
    })
    if(this.data.page == 0){
    this.selectCanteen(this.data.xianlinArr[event.detail.value].id)
    } else{
      this.selectCanteen(this.data.gulouArr[event.detail.value].id)
    }

  },

  onTabsClick1(){

  },

  onTabsClick(event) {
  
    
  },

  onStickyScroll(event) {
    console.log(event.detail);
  },

  handleTap: function (e) {
    const { clientX, clientY } = e.touches[0];
    console.log("点击坐标：", clientX, clientY);
    this.selectCanteen(1);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  
  onLoad(options) {
    var that = this
    wx.request({
      url: 'http://localhost:9998/canteen',
      method: 'GET',
      success: function (res) {

        // 过滤仙林校区的数据
        const xianlinArr = res.data.result.filter(item => item.school === "仙林校区");
        // 过滤鼓楼校区的数据
        const gulouArr = res.data.result.filter(item => item.school === "鼓楼校区");
        that.selectCanteen(xianlinArr[0].id)
        that.setData({
          xianlinArr:xianlinArr,
          gulouArr:gulouArr
        })
      }
    })
  
    

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },
  
     

     
   })



 

