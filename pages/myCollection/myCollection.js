// pages/myCollection/myCollection.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    detailStorage:{},
    nickName:'',
    canteenName:[],
    prices:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const userInfo = wx.getStorageSync('userInfo');
    if(userInfo){
      const{avatarUrl,nickName} = userInfo;
      this.setData({
        avatarUrl,
        nickName
      })
    }
    var that = this
    var save = this
    let prices = []
    wx.request({
      url: 'http://localhost:9998/user/star/' + that.data.nickName,
      method: 'GET',
      success: function (res) {
        that.setData({
          dishes: res.data.result
        })

        res.data.result.forEach(element=>{
          prices.push(element.score.toFixed(1))
      })
      that.setData({
        prices: prices
      })

      let canteenName = []
        wx.request({
          url: 'http://localhost:9998/canteen',
          method: 'GET',
          success: function (res) {
            console.log(save)
            save.data.dishes.forEach(element => {
              canteenName.push(res.data.result[element.cid - 1].name)
            });
            save.setData({
              canteenName:canteenName
            })
            
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    var that = this
    wx.request({
      url: 'http://localhost:9998/user/star/' + that.data.nickName,
      method: 'GET',
      success: function (res) {
        that.setData({
          dishes: res.data.result
        })
      }
    })
  }
})