// pages/publish/publish.js

Page({

    /**
     * 页面的初始数据
     */
    data: {
        //展示给用户的数据
        multiArray: [
            [0,1,2,3,4,5],
            [0,1,2,3,4,5,6,7,8,9]
        ],
        multiIndex: [0, 0, 0],
        //储备数据
        pickerList:[
            [0,1,2,3,4,5,6,7,8,9],
            [0,1,2,3,4,5,6,7,8,9],
            [0,1,2,3,4,5,6,7,8,9],
            [0,1,2,3,4,5,6,7,8,9],
            [0,1,2,3,4,5,6,7,8,9],
            [0]

        ],
        select: false,
        desc:" ",
        score: 0.0,
        did: 0,
        star: ['../../images/star_grey.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png'],
        tap: [false,false, false,false,false]
    
    },
  
    toPublish(){
      let num1 = this.data.multiArray[0][this.data.multiIndex[0]];
      let num2 = this.data.multiArray[1][this.data.multiIndex[1]]
      let score = num1 + num2 * 0.1;
      const uid = wx.getStorageSync('uid');
      const userInfo = wx.getStorageSync('userInfo');
      let avatar = "";
      var that = this
      if(userInfo){
        const{avatarUrl,nickName} = userInfo;
        this.setData({
          avatarUrl,
          nickName
        })
      }else{
        this.setData({
          nickName : "用户未登录"
        })
      }

    wx.request({
      url: 'http://localhost:9998/user/avatar/'+ userInfo.nickName,
      method: 'GET',
      success: function (res) {
        console.log(res.data.result)
        avatar = res.data.result;
        wx.request({
          url: 'http://localhost:9998/comment/add',
          method: 'POST',
          data: {
            "uid": uid,
            "username": that.data.nickName,
            "did": that.data.did,
            "score": that.data.score,
            "content":that.data.desc,
            "avatarUrl":avatar
          },
          
          success: function (res) {
            console.log(that)
            wx.navigateBack({
              delta: 1,
              success: () =>{
                  wx.showToast({
                      icon: 'none',
                    title: '发布成功',
                  })
              }
            })
          }
        })
      }

    })

    

    //更新评分
    wx.request({
      url: 'http://localhost:9998/update',
      success: function(res){
      }
    })
      },

    selectType(e){
        const { id } = e.currentTarget.dataset;
        this.setData({
            type: id
        })

    },

    getDesc(e){
        this.setData({
            desc:e.detail.value
        })
    },

    bindMultiPickerChange(e){
        this.setData({
            select: true
        })
    },

    bindMultiPickerColumnChange(e){
        let { column, value} = e.detail;
        let data = this.data;
        let {multiArray, pickerList} = this.data;
        if(column == 0){
            multiArray[1] = pickerList[value];
        }
        data.multiArray = multiArray;
        data.multiIndex[column] = value;
        this.setData(data);

        
    },

    closeSelect(){
        this.setData({
            select: false,
            multiIndex: [0,0],
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
          did: JSON.parse(options.id)
        })
  
    },
    one(){
      if(this.data.tap[0]==true && this.data.tap[1]==false){
        this.setData({
          star: ['../../images/star_grey.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png'],
          tap: [false,false,false,false,false],
          score: 0.0
        })
      }
      else{
         this.setData({
          star: ['../../images/star_yellow.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png'],
          tap: [true,false,false,false,false],
          score: 1.0
      })
      }
     

      
    },
    two(){
      if(this.data.tap[1]==true && this.data.tap[2]==false){
        this.setData({
          star: ['../../images/star_yellow.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png'],
          tap: [true,false,false,false,false],
          score: 1.0
        })
      }
      else{
        this.setData({
          star: ['../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png'],
          tap: [true,true,false,false,false],
          score: 2.0
      })
      }
      
      
      
    },
    three(){
      if(this.data.tap[2]==true && this.data.tap[3]==false){
        this.setData({
          star: ['../../images/star_yellow.png',
                '../../images/star_yellow.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png'],
          tap: [true,true,false,false,false],
          score: 2.0
        })
      }
      else{
        this.setData({
          star: ['../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_grey.png',
              '../../images/star_grey.png'],
          tap: [true,true,true,false,false],
          score: 3.0
      })
      }
      
    },
    four(){
      if(this.data.tap[3]==true && this.data.tap[4]==false){
        this.setData({
          star: ['../../images/star_yellow.png',
                '../../images/star_yellow.png',
                '../../images/star_yellow.png',
                '../../images/star_grey.png',
                '../../images/star_grey.png'],
          tap: [true,true,true,false,false],
          score: 3.0
        })
      }
      else{
        this.setData({
          star: ['../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_grey.png'],
          tap: [true,true,true,true,false],
          score: 4.0
      })
      }
     
    },
    five(){
      if(this.data.tap[4]==true){
        this.setData({
          star: ['../../images/star_yellow.png',
                '../../images/star_yellow.png',
                '../../images/star_yellow.png',
                '../../images/star_yellow.png',
                '../../images/star_grey.png'],
          tap: [true,true,true,true,false],
          score: 4.0
        })
      }
      else{
        this.setData({
          star: ['../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_yellow.png',
              '../../images/star_yellow.png'],
          tap: [true,true,true,true,true],
          score: 5.0
      })
      }
      
    }
})