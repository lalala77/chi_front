

const app = getApp()
Page({
  data: {
  
    dishes: [],
    blocks: [{ padding: '10px', background: 'cornflowerblue' }],
    prizes: [
      { fonts: [{ text: '0', top: '20%' }], background: '#e9e8fe' },
      { fonts: [{ text: '1', top: '10%' }], background: '#b8c5f2' },
    ],
    buttons: [
      { radius: '50px', background:  'cornflowerblue'},
      { radius: '45px', background: 'white' },
      {
        radius: '40px', background: 'cornflowerblue',
        pointer: true,
        fonts: [{ text: 'Start!', top: '-10px' }]
      },
    ],
    count: 5
  },

    /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const params = JSON.parse(options.params);
    console.log(params); // 打印接收到的数组参数
    this.setData({
      dishes:params
    })
    const prizes = params.map(item => {
      return {
        fonts: [{ text: item.name, top: '20%' }],
        background: this.getColor(),
      };
    });
    this.setData({
      prizes:prizes,
    })
    this.data.count = params.length
    
  },

  getNum(){
   return  Math.floor(Math.random() * this.data.count);
  },

  getColor(){
    const morandiColors = [
      "#F6CED8", // 淡粉色
      "#B0C4DE", // 淡蓝色
      "#E6E6FA", // 淡紫色
      "#FFFACD", // 淡黄色
      "#90EE90", // 淡绿色
      "#FFE4B5", // 淡橘色
      "#AFEEEE"  // 淡青色
    ];
    const randomIndex = Math.floor(Math.random() * morandiColors.length);
    return morandiColors[randomIndex];
  },

  jumpToIndex(){
    console.log("lalal")
    wx.reLaunch({
      url: '/pages/index/index',
    })
  },

  start () {
    
    // 获取抽奖组件实例
    const child = this.selectComponent('#myLucky')
    // 调用play方法开始旋转
    child.lucky.play()
    // 用定时器模拟请求接口
    const index =  this.getNum();
    setTimeout(() => {
      // 3s 后得到中奖索引 (假设抽到第0个奖品)
      // 调用stop方法然后缓慢停止
      child.lucky.stop(index)
    }, 3000)
  },
  end (event) {
    // 中奖奖品详情
    console.log("结束了")
  }
})
